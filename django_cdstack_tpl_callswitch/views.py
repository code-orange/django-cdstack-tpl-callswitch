from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    get_host_vars,
    zip_add_file,
)
from django_cdstack_models.django_cdstack_models.models import CmdbHost
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_deb_buster.django_cdstack_tpl_deb_buster.views import (
    handle as handle_deb_buster,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_callswitch/django_cdstack_tpl_callswitch"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    all_mediasw_nodes = CmdbHost.objects.filter(cmdbhostgroups__group_rel=2)

    ext_gws = list()

    for key in template_opts.keys():
        if key.startswith("sip_gw_ext_peer_") and key.endswith("_name"):
            key_ident = key[:-5]

            gw_conf = dict()
            gw_conf["sip_gw_name"] = template_opts[key_ident + "_name"]
            gw_conf["sip_gw_realm"] = template_opts[key_ident + "_realm"]
            gw_conf["sip_gw_proxy"] = template_opts[key_ident + "_proxy"]
            gw_conf["sip_gw_profile"] = template_opts[key_ident + "_profile"]

            if key_ident + "_auth_user" in template_opts:
                gw_conf["sip_gw_auth_username"] = template_opts[
                    key_ident + "_auth_user"
                ]

            if key_ident + "_auth_passwd" in template_opts:
                gw_conf["sip_gw_auth_passwd"] = template_opts[
                    key_ident + "_auth_passwd"
                ]

            ext_gws.append(gw_conf)

    template_opts["ext_gws"] = ext_gws
    template_opts["ext_gws_count"] = len(ext_gws)
    template_opts["int_gws"] = list()
    template_opts["int_gws_count"] = len(all_mediasw_nodes)

    for mediasw_node in all_mediasw_nodes:
        template_opts["int_gws"].append(get_host_vars(mediasw_node))

    for mediasw_node in all_mediasw_nodes:
        special_opts = get_host_vars(mediasw_node)

        special_opts["sip_gw_name"] = special_opts["node_hostname"]
        special_opts["sip_gw_realm"] = special_opts["network_iface_eth1_ip"]
        special_opts["sip_gw_proxy"] = special_opts["network_iface_eth1_ip"]
        special_opts["sip_gw_auth_username"] = (
            "callswitch-" + template_opts["node_hostname"]
        )
        special_opts["sip_gw_auth_passwd"] = template_opts["sip_gw_auth_passwd"]

        config_template_file = open(
            module_prefix
            + "/templates/config-fs/dynamic/etc/freeswitch/sip_profiles/internal/register-auth.xml",
            "r",
        ).read()
        config_template = django_engine.from_string(config_template_file)

        zip_add_file(
            zipfile_handler,
            "etc/freeswitch/sip_profiles/internal/"
            + special_opts["node_hostname"]
            + ".xml",
            config_template.render(special_opts),
        )

    for gw in ext_gws:
        config_template_file = open(
            module_prefix
            + "/templates/config-fs/dynamic/etc/freeswitch/sip_profiles/external/"
            + gw["sip_gw_profile"]
            + ".xml",
            "r",
        ).read()

        config_template = django_engine.from_string(config_template_file)

        zip_add_file(
            zipfile_handler,
            "etc/freeswitch/sip_profiles/external/" + gw["sip_gw_name"] + ".xml",
            config_template.render(gw),
        )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_whclient(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if not skip_handle_os:
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
